/*
 * Author: Warot Prapatsorn
 * This is a class that represents an MLP(multi layer percentron)
 * It can be train using normal back propagation algorithm and sweed pseudo
 * back propagation algorithm.
 *
 */

package cosc420asgn01;

import java.util.ArrayList;
import java.lang.Math;

public class Network {

    int inputUnits;
    int hiddenUnits;
    int outputUnits;
    int globalCount = 0;
    ArrayList<Units> Ainput = new ArrayList<>();
    ArrayList<Units> Ahidden = new ArrayList<>();
    ArrayList<Units> Aoutput = new ArrayList<>();
    
    // intialize with size of unit in each layer input, hidden and output 
    public Network(int inputUnits, int hiddenUnits, int outputUnits) {
        this.inputUnits = inputUnits;
        this.hiddenUnits = hiddenUnits;
        this.outputUnits = outputUnits;
        initialize();
    }
    
    //intialize when copying a network
    public Network(int inputUnits, int hiddenUnits, int outputUnits, ArrayList<Units> Ainput,
            ArrayList<Units> Ahidden, ArrayList<Units> Aoutput) {
        this.inputUnits = inputUnits;
        this.hiddenUnits = hiddenUnits;
        this.outputUnits = outputUnits;
        this.Ainput = Ainput;
        this.Ahidden = Ahidden;
        this.Aoutput = Aoutput;
    }

    public void initialize() {
        Ainput.clear();
        Ahidden.clear();
        Aoutput.clear();
        globalCount = 0;

        for (int i = 0; i < inputUnits; i++) {
            double sign = Math.random();

            Units input = new Units(globalCount, 0);
            globalCount++;
            Ainput.add(input);
        }

        for (int i = 0; i < hiddenUnits; i++) {
            //weights are set to random between -0.3 to 0.3
            double sign = Math.random();
            if (sign > 0.5) {
                sign = 1;
            } else {
                sign = -1;
            }
            Units hidden = new Units(globalCount, Math.random() * 0.3 * sign);
            globalCount++;
            Ahidden.add(hidden);
        }

        for (int i = 0; i < outputUnits; i++) {
            double sign = Math.random();
            if (sign > 0.5) {
                sign = 1;
            } else {
                sign = -1;
            }
            Units output = new Units(globalCount, Math.random() * 0.3 * sign);
            globalCount++;
            Aoutput.add(output);
        }
        //Initialize all connection
        for (int i = 0; i < hiddenUnits; i++) {
            for (int j = 0; j < inputUnits; j++) {
                double sign = Math.random();
                if (sign > 0.5) {
                    sign = 1;
                } else {
                    sign = -1;
                }

                Units unit1 = Ahidden.get(i);
                Units unit2 = Ainput.get(j);

                unit1.setConnection(unit2, Math.random() * 0.3 * sign);

            }
        }

        for (int i = 0; i < outputUnits; i++) {
            for (int j = 0; j < hiddenUnits; j++) {
                double sign = Math.random();
                if (sign > 0.5) {
                    sign = 1;
                } else {
                    sign = -1;
                }
                Units unit1 = Aoutput.get(i);
                Units unit2 = Ahidden.get(j);
                unit1.setConnection(unit2, Math.random() * 0.3 * sign);
            }
        }
    }

    public double activationFunction(double input) {
        double answer = 1 / (1 + Math.pow(Math.E, -input));
        return answer;
    }

    //Calculate the population error given an arraylist of error for each pattern
    public double ErorPopulation(ArrayList<Double> paternE, int output, int pattern) {
        double answer = 0;

        for (int i = 0; i < paternE.size(); i++) {
            answer += paternE.get(i);

        }
        //answer = answer / (Aoutput.size() * teachers.size());
        answer = answer / (output * pattern);

        return answer;
    }

    public void printANN() {

        for (int i = 0; i < inputUnits; i++) {
            System.out.println(Ainput.get(i).getName() + " bias = "
                    + Ainput.get(i).getBias() + " input = " + Ainput.get(i).getInput()
                    + " momentum error " + Ainput.get(i).getPassSumError());
        }

        for (int i = 0; i < hiddenUnits; i++) {
            System.out.println(Ahidden.get(i).getName() + " bias = "
                    + Ahidden.get(i).getBias() + " momentum error " + Ahidden.get(i).getPassSumError());
            ArrayList<Connection> con = Ahidden.get(i).getConnection();
            for (int m = 0; m < con.size(); m++) {
                System.out.println(con.get(m).getName() + " weights = "
                        + con.get(m).getWeights() + " momentum error " + con.get(m).getPassSumerror());
            }
        }
        for (int i = 0; i < outputUnits; i++) {
            System.out.println(Aoutput.get(i).getName() + " bias = "
                    + Aoutput.get(i).getBias() + " momentum error " + Aoutput.get(i).getPassSumError());
            ArrayList<Connection> con = Aoutput.get(i).getConnection();
            for (int m = 0; m < con.size(); m++) {
                System.out.println(con.get(m).getName() + " weights = "
                        + con.get(m).getWeights() + " momentum error " + con.get(m).getPassSumerror());
            }
        }
    }

    public ArrayList<Double> oneForward(ArrayList<Double> Tinput) {

        for (int a = 0; a < Tinput.size(); a++) {
            Ainput.get(a).setInput(Tinput.get(a));
        }
        for (int i = 0; i < Ahidden.size(); i++) {
            ArrayList<Connection> Acon = Ahidden.get(i).getConnection();
            double sum = 0;
            for (int j = 0; j < Acon.size(); j++) {
                Connection con = Acon.get(j);
                //Units node1 = con.getUnit1();
                Units node2 = con.getUnit2();
                double preinput = node2.getInput();
                double weights = con.getWeights();
                sum += preinput * weights;

            }

            Units node1 = Ahidden.get(i);
            sum = sum + node1.getBias();
            sum = activationFunction(sum);

            node1.setInput(sum);

        }

        for (int i = 0; i < Aoutput.size(); i++) {
            ArrayList<Connection> Acon = Aoutput.get(i).getConnection();
            double sum = 0;
            for (int j = 0; j < Acon.size(); j++) {
                Connection con = Acon.get(j);

                Units node2 = con.getUnit2();
                double preinput = node2.getInput();
                double weights = con.getWeights();
                sum += preinput * weights;
            }
            Units node1 = Aoutput.get(i);
            sum = sum + node1.getBias();

            sum = activationFunction(sum);
            node1.setInput(sum);

        }

        ArrayList<Double> answer = new ArrayList<>();
        // System.out.print("The output is ");
        for (int i = 0; i < outputUnits; i++) {
            // System.out.print(Aoutput.get(i).getInput() + " ");
            answer.add(Aoutput.get(i).getInput());
        }
        //System.out.println();

        return answer;
    }

    public Network backProbTwoNet(double lC, double mO, double eR,
            ArrayList<Double> iP, ArrayList<Double> tH, Network prevNet) {

        int epoch = 0;
        double Ecriteria = 100;
        int restart = 0;
        int bufferSize = 5;
        ArrayList<ArrayList<Double>> inputs = new ArrayList<>();
        ArrayList<ArrayList<Double>> teachers = new ArrayList<>();

        inputs.add(iP);
        teachers.add(tH);
        for (int i = 0; i < bufferSize; i++) {
            ArrayList<Double> pseudoInputs = new ArrayList<>();
            for (int j = 0; j < inputUnits; j++) {
                double oneOfTheInput = Math.random();
                /*
                 if (oneOfTheInput > 0.5) {
                 oneOfTheInput = 0.0;
                 } else {
                 oneOfTheInput = 1.0;
                 }*/
                pseudoInputs.add(oneOfTheInput);
            }
            ArrayList<Double> pseudoTeacher = prevNet.oneForward(pseudoInputs);
            inputs.add(pseudoInputs);
            teachers.add(pseudoTeacher);
        }

        ArrayList<Double> patternE = new ArrayList<>();
        double learingConstant = lC;
        double momentum = mO;

        double EcriteriaRange = eR;

        while (true) {
            // System.out.println(inputs);
            // System.out.println(teachers);

            for (int k = 0; k < inputs.size(); k++) {
                ArrayList<Double> Tinput = inputs.get(k);

                feedForward(Tinput);
                //System.out.println();
                ArrayList<Double> cteacher = teachers.get(k);
                double Tteacher = 0;
                int checkCount = Aoutput.size();
                patternE.add(feedback(cteacher, learingConstant, momentum, true));

            }

            Ecriteria = ErorPopulation(patternE, outputUnits, teachers.size());
            epoch++;
            patternE.clear();

            //if the network is taking more than 50k iteration, intialize the network again and restart backProb.
            if (epoch > 50000) {
               // System.out.println("The network is taking too long just update");

                //backProbTwoNet(lC, mO, eR, iP, tH, prevNet);
                // prevNet.printANN();
                //System.out.println("Population Error is " + Ecriteria
                //  + ", Epoch = " + epoch);
                return this;

                //restart++;
                //initialize();
                //backProbTwoNet()
            }

            //sum up all the weights at the end
            updateError(Ecriteria, EcriteriaRange, momentum);
            if (Ecriteria < EcriteriaRange) {
                break;
            }
            inputs.clear();
            teachers.clear();
            inputs.add(iP);
            teachers.add(tH);
            for (int i = 0; i < bufferSize; i++) {
                ArrayList<Double> pseudoInputs = new ArrayList<>();
                for (int j = 0; j < inputUnits; j++) {
                    double oneOfTheInput = Math.random();
                    /*
                     if (oneOfTheInput > 0.5) {
                     oneOfTheInput = 0.0;
                     } else {
                     oneOfTheInput = 1.0;
                     }
                     */
                    pseudoInputs.add(oneOfTheInput);
                }

                ArrayList<Double> pseudoTeacher = prevNet.oneForward(pseudoInputs);

                inputs.add(pseudoInputs);
                teachers.add(pseudoTeacher);
            }

        }
        /*  System.out.println("Population Error is " + Ecriteria
         + ", Epoch = " + epoch);*/

        return this;
    }

    public double check(ArrayList<ArrayList<Double>> inputs, ArrayList<ArrayList<Double>> teachers) {
        int correct = 0;

        ArrayList<Double> patEarray = new ArrayList<>();
        // ArrayList<Double> sqrtEarray = new ArrayList<>();
        ArrayList<Double> goodEarray = new ArrayList<>();
        for (int k = 0; k < inputs.size(); k++) {
            ArrayList<Double> Tinput = inputs.get(k);

            feedForward(Tinput);
            ArrayList<Double> ONEteacher = teachers.get(k);
            int count = ONEteacher.size();
            double patE = 0;
            ArrayList<Double> vector1 = new ArrayList<>();
            ArrayList<Double> vector2 = new ArrayList<>();
            for (int i = 0; i < ONEteacher.size(); i++) {
                double output = Aoutput.get(i).getInput();
                double teacher = ONEteacher.get(i);
                patE += Math.pow(output - teacher, 2);
                if (Math.sqrt(Math.pow(ONEteacher.get(i) - Aoutput.get(i).getInput(), 2)) < 0.2) {
                    count -= 1;
                }
                output = (output * 2) - 1;
                teacher = (teacher * 2) - 1;
                vector1.add(output);
                vector2.add(teacher);
            }
            double goodness = 0;
            for (int ca = 0; ca < vector1.size(); ca++) {
                double output = vector1.get(ca);
                double teacher = vector2.get(ca);
                goodness += output * teacher;

            }

            goodEarray.add(goodness / vector1.size());
            patEarray.add(patE);
            // sqrtEarray.add(patE);
            if (count == 0) {
                correct += 1;
            } else {
                /*
                 System.out.print("The teacher is ");
                 for (int i = 0; i < ONEteacher.size(); i++) {
                 System.out.print(ONEteacher.get(i).toString() + " ");
                 }
                 System.out.println();

                 System.out.print("The output is ");
                 for (int b = 0; b < Aoutput.size(); b++) {
                 System.out.print(Aoutput.get(b).getInput() + " ");
                 }
                 System.out.println();
                 */
            }

        }
        //  System.out.println(
        // "Population Error is " + ErorPopulation(patEarray, outputUnits, teachers.size()));

        // System.out.println("The ANN makes " + correct + " correct generalization out of " + inputs.size());
        double fly = 0;
        /* for (int h = 0; h < sqrtEarray.size(); h++) {
         fly += sqrtEarray.get(h);

         }*/
        double meanGoodness = 0;
        for (int cb = 0; cb < goodEarray.size(); cb++) {
            meanGoodness += goodEarray.get(cb);
        }
        //  System.out.println("The mean goodness is "+meanGoodness/goodEarray.size());
        //System.out.println("root mean square error = "+Math.sqrt(fly/teachers.size()));
        //System.out.println("popE = " + ErorPopulation(patEarray, outputUnits, teachers.size()));
        // return ErorPopulation(patEarray, outputUnits, teachers.size());

        return meanGoodness / goodEarray.size();
    }

    public Network copy() {

        globalCount = 0;
        ArrayList<Units> copyAinput = new ArrayList<>();
        ArrayList<Units> copyAhidden = new ArrayList<>();
        ArrayList<Units> copyAoutput = new ArrayList<>();
        for (int i = 0; i < inputUnits; i++) {

            Units input = new Units(globalCount, 0);
            globalCount++;
            copyAinput.add(input);
        }
        for (int i = 0; i < hiddenUnits; i++) {
            double copyBias = Ahidden.get(i).getBias();
            Units hidden = new Units(globalCount, copyBias);
            globalCount++;
            copyAhidden.add(hidden);
        }
        for (int i = 0; i < outputUnits; i++) {
            double copyBias = Aoutput.get(i).getBias();
            Units output = new Units(globalCount, copyBias);
            globalCount++;
            copyAoutput.add(output);
        }
        for (int i = 0; i < hiddenUnits; i++) {
            ArrayList<Connection> conList = Ahidden.get(i).getConnection();
            for (int j = 0; j < inputUnits; j++) {
                Connection con = conList.get(j);
                double copyBias = con.getWeights();
                Units unit1 = copyAhidden.get(i);
                Units unit2 = copyAinput.get(j);

                unit1.setConnection(unit2, copyBias);

            }
        }

        for (int i = 0; i < outputUnits; i++) {
            ArrayList<Connection> conList = Aoutput.get(i).getConnection();
            for (int j = 0; j < hiddenUnits; j++) {
                Connection con = conList.get(j);
                double copyBias = con.getWeights();
                Units unit1 = copyAoutput.get(i);
                Units unit2 = copyAhidden.get(j);
                unit1.setConnection(unit2, copyBias);
            }
        }
        Network newNet = new Network(this.getInputUnits(), this.getHiddenUnits(), this.getOutUnits(), copyAinput,
                copyAhidden, copyAoutput);
        return newNet;
    }

    public int getInputUnits() {
        int newInputUnits = inputUnits;
        return newInputUnits;
    }

    public int getHiddenUnits() {
        int newHiddenUnits = hiddenUnits;
        return newHiddenUnits;
    }

    public int getOutUnits() {
        int newOutputUnits = outputUnits;
        return newOutputUnits;
    }

    public void feedForward(ArrayList<Double> Tinput) {
        for (int a = 0; a < Tinput.size(); a++) {
            Ainput.get(a).setInput(Tinput.get(a));
        }
        for (int i = 0; i < Ahidden.size(); i++) {
            ArrayList<Connection> Acon = Ahidden.get(i).getConnection();
            double sum = 0;
            for (int j = 0; j < Acon.size(); j++) {
                Connection con = Acon.get(j);
                //Units node1 = con.getUnit1();
                Units node2 = con.getUnit2();
                double preinput = node2.getInput();
                double weights = con.getWeights();
                sum += preinput * weights;

            }

            Units node1 = Ahidden.get(i);
            sum = sum + node1.getBias();
            sum = activationFunction(sum);

            node1.setInput(sum);

        }

        for (int i = 0; i < Aoutput.size(); i++) {
            ArrayList<Connection> Acon = Aoutput.get(i).getConnection();
            double sum = 0;
            for (int j = 0; j < Acon.size(); j++) {
                Connection con = Acon.get(j);

                Units node2 = con.getUnit2();
                double preinput = node2.getInput();
                double weights = con.getWeights();
                sum += preinput * weights;
            }
            Units node1 = Aoutput.get(i);
            sum = sum + node1.getBias();

            sum = activationFunction(sum);
            node1.setInput(sum);

        }
    }

    public double feedback(ArrayList<Double> Tteacher, double learingConstant, double momentum, boolean batch) {
        double T = 0;
        for (int i = 0; i < Aoutput.size(); i++) {

            double output = Aoutput.get(i).getInput();
            double CurrentTeacher = Tteacher.get(i);
            T += Math.pow(CurrentTeacher - output, 2);
            double error = (CurrentTeacher - output) * output * (1 - output);

            Units node1 = Aoutput.get(i);

            node1.setError(error);

            double biasE = learingConstant * error * 1.0;
            if (!batch) {
                node1.setBias(node1.getBias() + biasE + (node1.getPassSumError() * momentum));
                node1.setBiasSumError(biasE + (node1.getBiasSumError() * momentum));
            } else {
                node1.setBiasSumError(node1.getBiasSumError() + biasE);
            }
        }

        for (int i = 0; i < Aoutput.size(); i++) {
            ArrayList<Connection> Acon = Aoutput.get(i).getConnection();
            for (int t = 0; t < Acon.size(); t++) {

                Connection con = Acon.get(t);
                Units node1 = con.getUnit1();
                Units node2 = con.getUnit2();

                double error = learingConstant * node1.getError() * node2.getInput();
                if (!batch) {
                    con.setWeights(con.getWeights() + error + (con.getSumError() * momentum));
                    con.setSumError(error + (con.passSumerror * momentum));
                } else {
                    //error = con.getSumError() + error;
                    con.setSumError(con.getSumError() + error);
                }
                node2.setError(node2.getError() + (node1.getError() * con.getWeights()));
            }
        }

        for (int i = 0; i < Ahidden.size(); i++) {
            Units node1 = Ahidden.get(i);
            double error = node1.getError()
                    * (node1.getInput() * (1 - node1.getInput()));

            node1.setError(error);
            double biasE = error * 1.0 * learingConstant;
            if (!batch) {
                node1.setBias(node1.getBias() + biasE + (node1.getBiasSumError() * momentum));
                node1.setBiasSumError(error + (node1.getBiasSumError() * momentum));
            } else {
                node1.setBiasSumError(node1.getBiasSumError() + biasE);
            }
        }

        for (int i = 0; i < Ahidden.size(); i++) {
            ArrayList<Connection> Acon = Ahidden.get(i).getConnection();
            for (int t = 0; t < Acon.size(); t++) {

                Connection con = Acon.get(t);
                Units node1 = con.getUnit1();
                Units node2 = con.getUnit2();
                double error = learingConstant * node1.getError() * node2.getInput();
                if (!batch) {
                    con.setWeights(con.getWeights() + error + (con.getPassSumerror() * momentum));
                    con.setPassSumerror(error + (con.getPassSumerror() * momentum));
                } else {
                    error = con.getSumError() + error;
                    con.setSumError(error);
                }
            }
        }
        return T;
    }

    public void backProb(double lC, double mO, double eR,
            ArrayList<ArrayList<Double>> iP, ArrayList<ArrayList<Double>> tH, boolean batch) {
        int epoch = 0;
        double Ecriteria = 100;
        int restart = 0;
        int randomnumber;
        ArrayList<Double> patternE = new ArrayList<>();
        ArrayList<Double> Tinput;
        double learingConstant = lC;
        double momentum = mO;
        ArrayList<Integer> random1 = new ArrayList<>();
        double EcriteriaRange = eR;
        while (true) {
            // printANN();
            for (int f = 0; f < iP.size(); f++) {

                if (!batch) {

                    randomnumber = (int) (Math.random() * iP.size());
                    while (random1.contains(randomnumber)) {
                        randomnumber = (int) (Math.random() * iP.size());
                    }
                    random1.add(randomnumber);
                    Tinput = iP.get(randomnumber);
                    // System.out.println(randomnumber);
                } else {
                    Tinput = iP.get(f);
                    randomnumber = -1;
                }
                feedForward(Tinput);
                double T = 0;
                ArrayList<Double> Tteacher;
                if (!batch) {
                    Tteacher = tH.get(randomnumber);
                } else {
                    Tteacher = tH.get(f);
                }
                patternE.add(feedback(Tteacher, learingConstant, momentum, batch));
            }

            Ecriteria = ErorPopulation(patternE, outputUnits, tH.size());

            patternE.clear();
            // System.out.println("Population Error is " + Ecriteria
            //       + ", Epoch = " + epoch);
            epoch++;
            //if the network is taking more than 50k iteration, intialize the network again and restart backProb.
            if (epoch == 50000) {
                System.out.println("The network is taking long...update");
                Ecriteria = EcriteriaRange - 1;
            }

            //sum up all the weights at the end and update 
            if (batch) {
                updateError(Ecriteria, EcriteriaRange, momentum);
            } else {
                random1.clear();
            }

            if (Ecriteria < EcriteriaRange) {

                break;
            }

        }

    }

    public void updateError(double Ecriteria, Double EcriteriaRange, double momentum) {
        for (int i = 0; i < hiddenUnits; i++) {
            if (Ecriteria > EcriteriaRange) {
                Ahidden.get(i).setBias(Ahidden.get(i).getBias() + checkSmall(Ahidden.get(i).getBiasSumError())
                        + (Ahidden.get(i).getPassSumError() * momentum));
                Ahidden.get(i).setPassSumError(checkSmall(Ahidden.get(i).getBiasSumError()) + (Ahidden.get(i).getPassSumError() * momentum));
                Ahidden.get(i).setBiasSumError(0);
            } else {
                Ahidden.get(i).setBiasSumError(0);
                Ahidden.get(i).setPassSumError(0);
            }
            ArrayList<Connection> con = Ahidden.get(i).getConnection();
            for (int m = 0; m < con.size(); m++) {
                if (Ecriteria > EcriteriaRange) {
                    con.get(m).setWeights(con.get(m).getWeights() + checkSmall(con.get(m).getSumError())
                            + (con.get(m).getPassSumerror() * momentum));
                    con.get(m).setPassSumerror(checkSmall(con.get(m).getSumError()) + (con.get(m).getPassSumerror() * momentum));
                    con.get(m).setSumError(0);
                } else {
                    con.get(m).setSumError(0);
                    con.get(m).setPassSumerror(0);
                }
            }

        }

        for (int i = 0; i < outputUnits; i++) {
            if (Ecriteria > EcriteriaRange) {
                Aoutput.get(i).setBias(Aoutput.get(i).getBias()
                        + checkSmall(Aoutput.get(i).getBiasSumError()) + (Aoutput.get(i).getPassSumError() * momentum));
                Aoutput.get(i).setPassSumError(checkSmall(Aoutput.get(i).getBiasSumError()) + (Aoutput.get(i).getPassSumError() * momentum));
                Aoutput.get(i).setBiasSumError(0);
            } else {
                Aoutput.get(i).setBiasSumError(0);
                Aoutput.get(i).setPassSumError(0);
            }
            ArrayList<Connection> con = Aoutput.get(i).getConnection();
            for (int m = 0; m < con.size(); m++) {
                if (Ecriteria > EcriteriaRange) {
                    con.get(m).setWeights(con.get(m).getWeights() + checkSmall(con.get(m).getSumError())
                            + (momentum * con.get(m).getPassSumerror()));
                    con.get(m).setPassSumerror(checkSmall(con.get(m).getSumError()) + (momentum * con.get(m).getPassSumerror()));
                    con.get(m).setSumError(0);
                } else {
                    con.get(m).setSumError(0);
                    con.get(m).setPassSumerror(0);
                }
            }

        }
    }

    public Network backProbPseudoOneInput(double lC, double mO, double eR,
            ArrayList<Double> iP, ArrayList<Double> tH, int pseudoSize, boolean batch, ArrayList<ArrayList<ArrayList<Double>>> pusedoInputandTeacher
    ) {
        ArrayList<ArrayList<Double>> iPP = new ArrayList<>();
        ArrayList<ArrayList<Double>> tHH = new ArrayList<>();
        iPP.add(iP);
        tHH.add(tH);
        ArrayList pseudoIn;
        ArrayList pseudoOut;
        if (pusedoInputandTeacher != null) {
            pseudoIn = pusedoInputandTeacher.get(0);
            pseudoOut = pusedoInputandTeacher.get(1);
        } else {
            pseudoIn = null;
            pseudoOut = null;
        }
        return backProbPseudo(lC, mO, eR, iPP, tHH, pseudoSize, batch, pseudoIn, pseudoOut);

    }

    public Network backProbPseudoRepeat(double lC, double mO, double eR,
            ArrayList<Double> iP, ArrayList<Double> tH, int pseudoSize, ArrayList<ArrayList<Double>> baseIn,
            ArrayList<ArrayList<Double>> BaseTe, boolean batch) {
        /*  ArrayList<ArrayList<Double>> iPP = new ArrayList<>();
         ArrayList<ArrayList<Double>> tHH = new ArrayList<>();
         iPP.add(iP);
         tHH.add(tH);*/
        Network oldNet = copy();
        Network tempNet = copy();

        //Network tempNet = this.backProbPseudoOneInput(lC, mO, eR, iPP, tHH, pseudoSize);
        tempNet = tempNet.backProbPseudoOneInput(lC, mO, eR, iP, tH, pseudoSize, batch, null);
        int i = 0;
        double meangoodness = 0;
        while ((meangoodness = tempNet.check(baseIn, BaseTe)) < 0.88) {

            //System.out.println("Repseudo population");
            //System.out.println("This should be different everytime " + tempNet.check(baseIn, BaseTe));
            //System.out.println("restart " + i + " times with "+meangoodness);
            tempNet = oldNet.copy();
            //System.out.println(tempNet);
            tempNet = tempNet.backProbPseudoOneInput(lC, mO, eR, iP, tH, pseudoSize, batch, null);
            i++;
            if (i > 100) {
                System.out.println("fail to remember");
                break;
            }
        }
        //oldNet.printANN();

        return tempNet;

    }

    public ArrayList<ArrayList<ArrayList<Double>>> genPseudo(int pseudoSize) {
        ArrayList<ArrayList<ArrayList<Double>>> pusedoInputandTeacher = new ArrayList<>();
        ArrayList<ArrayList<Double>> pseudoIn = new ArrayList<>();
        ArrayList<ArrayList<Double>> pseudoOut = new ArrayList<>();
        for (int i = 0; i < pseudoSize; i++) {
            ArrayList OneofThepseudoInputs = new ArrayList<>();
            for (int j = 0; j < inputUnits; j++) {
                //double oneOfTheInput = Math.random();
                double oneOfTheInput = Math.random();
                /* if (oneOfTheInput > 0.5) {
                 oneOfTheInput = 0.0;
                 } else {
                 oneOfTheInput = 1.0;
                 }*/

                OneofThepseudoInputs.add(oneOfTheInput);
            }
            ArrayList<Double> OneofThePseudoTeacher = oneForward(OneofThepseudoInputs);
            pseudoIn.add(OneofThepseudoInputs);
            pseudoOut.add(OneofThePseudoTeacher);

        }

        pusedoInputandTeacher.add(pseudoIn);
        pusedoInputandTeacher.add(pseudoOut);
        return pusedoInputandTeacher;
    }

    //normal pseudo rehearsal
    public Network backProbPseudo(double lC, double mO, double eR,
            ArrayList<ArrayList<Double>> iP, ArrayList<ArrayList<Double>> tH, int pseudoSize, boolean batch, ArrayList<ArrayList<Double>> pseudoI,
            ArrayList<ArrayList<Double>> pseudoT) {

        int epoch = 0;
        double Ecriteria = 100;
        double passEcriteria = 0;
        int restart = 0;
        int bufferSize = 5;
        int counter = 0;

        ArrayList<ArrayList<Double>> inputs = new ArrayList<>();
        ArrayList<ArrayList<Double>> teachers = new ArrayList<>();
        ArrayList<ArrayList<Double>> pseudoIn = new ArrayList<>();
        ArrayList<ArrayList<Double>> pseudoOut = new ArrayList<>();
        int randomnumber;
        ArrayList<Integer> random1 = new ArrayList<>();
        ArrayList<Integer> random2 = new ArrayList<>();
        ArrayList<Integer> random3 = new ArrayList<>();

        for (int i = 0; i < iP.size(); i++) {
            inputs.add(iP.get(i));
            teachers.add(tH.get(i));
        }
        if (pseudoI == null && pseudoT == null) {
            ArrayList<ArrayList<ArrayList<Double>>> pusedoInputandTeacher = genPseudo(pseudoSize);
            pseudoIn = pusedoInputandTeacher.get(0);
            pseudoOut = pusedoInputandTeacher.get(1);
        } else {
            pseudoIn = pseudoI;
            pseudoOut = pseudoT;
        }
        for (int i = 0; i < bufferSize; i++) {
            randomnumber = (int) (Math.random() * pseudoIn.size());
            while (random1.contains(randomnumber)) {
                randomnumber = (int) (Math.random() * pseudoIn.size());
            }
            random1.add(randomnumber);
            inputs.add(pseudoIn.get(randomnumber));
            teachers.add(pseudoOut.get(randomnumber));
        }
        random1.clear();
        ArrayList<Double> patternE = new ArrayList<>();
        double learingConstant = lC;
        double momentum = mO;

        double EcriteriaRange = eR;

        while (true) {
            //System.out.println(inputs);
            //System.out.println(teachers);

            for (int k = 0; k < inputs.size(); k++) {
                ArrayList<Double> Tinput;
                if (!batch) {

                    randomnumber = (int) (Math.random() * inputs.size());
                    while (random2.contains(randomnumber)) {
                        randomnumber = (int) (Math.random() * inputs.size());
                    }

                    random2.add(randomnumber);
                    Tinput = inputs.get(randomnumber);

                } else {
                    Tinput = inputs.get(k);
                    randomnumber = -1;
                }

                feedForward(Tinput);
                ArrayList<Double> cteacher;
                if (!batch) {
                    cteacher = teachers.get(randomnumber);
                } else {
                    cteacher = teachers.get(k);
                }
                patternE.add(feedback(cteacher, learingConstant, momentum, batch));

            }

            Ecriteria = ErorPopulation(patternE, outputUnits, teachers.size());
            patternE.clear();
            epoch++;
            //if the network is taking more than 50k iteration, intialize the network again and restart backProb.
            //  System.out.println("Population Error is " + Ecriteria
            //        + ", Epoch = " + epoch);
            if (epoch > 50000) {
               // System.out.println("The network is taking too long just update");

                // prevNet.printANN();
                // System.out.println("Population Error is " + Ecriteria
                //        + ", Epoch = " + epoch);
                Ecriteria = EcriteriaRange - 1;

            }
            //sum up all the weights at the end
            if (!batch) {
                random2.clear();
            } else {
                updateError(Ecriteria, EcriteriaRange, momentum);
            }
            if (Ecriteria < EcriteriaRange) {
                break;
            }
            inputs.clear();
            teachers.clear();
            for (int i = 0; i < iP.size(); i++) {
                inputs.add(iP.get(i));
                teachers.add(tH.get(i));
            }

            for (int i = 0; i < bufferSize; i++) {
                int randomnumber3 = (int) (Math.random() * pseudoIn.size());
                while (random2.contains(randomnumber3)) {
                    randomnumber3 = (int) (Math.random() * pseudoIn.size());
                }
                random3.add(randomnumber3);

                inputs.add(pseudoIn.get(randomnumber3));
                teachers.add(pseudoOut.get(randomnumber3));
            }
            random3.clear();
        }

        return this;
    }

    public double checkSmall(double b) {
        /*
         if (b < 0) {
         Math.abs(b);
         if (b < 0.0001) {
         // System.out.println(b);
         b = -0.0001;
         }
         } else {
         if (b < 0.0001) {
         //System.out.println(b);
         b = 0.0001;
         }
         }
         */
        return b;
    }

    public void addHidden() {
        double sign = Math.random();
        if (sign > 0.5) {
            sign = 1;
        } else {
            sign = -1;
        }
        Units Newhidden = new Units(globalCount, Math.random() * 0.3 * sign);
        globalCount++;
        Ahidden.add(Newhidden);
        hiddenUnits++;
        for (int j = 0; j < inputUnits; j++) {
            sign = Math.random();
            if (sign > 0.5) {
                sign = 1;
            } else {
                sign = -1;
            }
            Units unit2 = Ainput.get(j);
            Newhidden.setConnection(unit2, Math.random() * 0.3 * sign);

        }
        for (int i = 0; i < outputUnits; i++) {
            sign = Math.random();
            if (sign > 0.5) {
                sign = 1;
            } else {
                sign = -1;
            }
            Units unit1 = Aoutput.get(i);
            unit1.setConnection(Newhidden, Math.random() * 0.3 * sign);
        }
    }
}
