/*
 * This class represents a unit in network class
 */

package cosc420asgn01;
import java.util.ArrayList;


public class Units {

    double bias;
    double input;
    String name;
    double error;
    double passSumError;

    public double getPassSumError() {
        double newPassSumError = passSumError;
        return newPassSumError;
    }
    
    public String getName(){
        String newName = name;
        return newName;
    } 
    public void setPassSumError(double passSumError) {
        this.passSumError = passSumError;
    }
    public double getBiasSumError() {
        double newBiasSumError = BiasSumError;
        return newBiasSumError;
    }

    public void setBiasSumError(double BiasSumError) {
        this.BiasSumError = BiasSumError;
    }
    
    double BiasSumError;

    public double getError() {
        double Newerror = error; 
        return Newerror;
    }

    public void setError(double error) {
        this.error = error;
    }
    ArrayList<Connection> forward = new ArrayList<>();
    //  ArrayList<Connection> backward = new ArrayList<>();

    public Units(int name, double bias) {
        this.name = Integer.toString(name);
        this.bias = bias;
        error = 0;
        input = 0;
        passSumError = 0;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public double getBias() {
        return bias;
    }

    public void setConnection(Units unit2, double weights) {
        Connection con1 = new Connection(this, unit2, weights);
        forward.add(con1);
    }

    public ArrayList<Connection> getConnection() {
        ArrayList<Connection> newForward = new ArrayList<>();
        for(int i = 0; i<forward.size();i++){
            Connection con = forward.get(i);
            
        }
        return forward;
    }
    public void setConnection(ArrayList<Connection> forward) {
        this.forward = forward;
    }
    public void setInput(double input){
        this.input = input;
    }
    
    public double getInput(){
        double newInput = input;
        return newInput;
    }
    public void setName(String name){
        this.name = name;
    }
  
    
}
