/*
 * This class represents a connection in network class
 */
package cosc420asgn01;
public class Connection {

    String name;
    double weights;

    double SumError;
    double passSumerror;

    public double getPassSumerror() {
        double newPassSumerror = passSumerror;
        return newPassSumerror;
    }

    public void setPassSumerror(double passSumerror) {
        this.passSumerror = passSumerror;
    }

    public double getWeights() {
        double newWeights = weights;
        return newWeights;
    }

    public void setWeights(double weights) {
        this.weights = weights;
    }

    public double getSumError() {
        double newSumError = SumError;
        return newSumError;
    }

    public void setSumError(double SumError) {
        this.SumError = SumError;
    }
    Units unit1;
    Units unit2;

    public Connection(Units unit1, Units unit2, double weights) {
        this.weights = weights;
        this.unit1 = unit1;
        this.unit2 = unit2;
        name = unit1.getName() + " - " + unit2.getName();
        SumError = 0;
        passSumerror = 0;
    }

    public String getName() {
        return name;
    }

    public Units getUnit1() {
        return unit1;
    }

    public Units getUnit2() {
        return unit2;
    }
}
